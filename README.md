# Breed Classification on Oxford Pets III Dataset with YOLOv5

Train YOLOv5 for breed classification on Oxford Pets III dataset, and server result through Dockerized implementation of a flask-based HTML/JS frontend and a API service on FastAPI.

## RUN INSTRUCTIONS

### Starting and Using the Services

Clone the repository and run "docker-compose up" on local machine (you will need a login to Docker Hub in order to pull the public images for this project).

On the local machine's browser, visit "http://localhost:5000".  Submit a photo of a pet (JPEG format, less than 1MB) and click submit.  You will see the picture with bounding box, breed and probability of prediction indicated.

To explore the functionality of the FastAPI backend, got to "http://localhost:8080/docs#" on the local machine.

## DESIGN DOCUMENT

## Solution Components

- **Roboflow_Train_YOLOv5_OxfordBreedID_MLFlow.ipynb:** Notebook for training model.  Trains YOLOv5 from scratch (without pretrained weights) using Oxford Pets III dataset (https://www.robots.ox.ac.uk/~vgg/data/pets/).  Trained weights are then saved and used in the FastAPI service for prediction.

- **yutee/flask-client-catdogbreedclf:** Public container image on Docker Hub. Serves a simple HTML/JS client for user interaction. Code and Dockerfile can be found in fastapi-yolov5-catdogbreedclf.zip.  Command to build the container: "docker build -t python-docker:latest ."  Built image is subsequently renamed using "docker tag" command and pushed to Docker Hub as a public image.

- **yutee/fastapi-yolov5-catdogbreedclf:** Public container image on Docker Hub. FastAPI backend with asynchronous calls. Serves asynchronous API for breed classification inference.  Command to build the container: "docker build -t yolov5-fastapi:0.0.1 ."  Built image is subsequently renamed using "docker tag" command and pushed to Docker Hub as a public image.

- **docker-compose.yml:** Defines and orchestrates the services and networking required for the solution.

### Dataset

- The dataset used (Oxford Pets III) in YOLOv5 format with head bounding box and breed labels are available as a public dataset (https://public.roboflow.com/object-detection/oxford-pets/1/download/yolov5pytorch.)  The dataset used is also included as a zip file in this repository, and can be used for testing.

- Labels contain breed classifier as well as coordinates specifying head bounding box.  The dataset comprises pictures of 25 dog breeds and 12 cat breeds.

- Training is from scratch.

### Architecture, Metrics and Losses.

- The model used is YOLOv5 ( https://github.com/ultralytics/yolov5).

- As documented in the notebook, training over 100 epochs, Mean Average Precision (MAP) at 0.5 of 0.81 and MAP at (0.5, 0.95) of 0.61 were achieved.  Losses converged smoothly for both training and validation.

### Model Training

- Full code required for training the model contained in the Jupyter notebook.  The Jupyter notebook was built on Google Colab Pro. 

- Training metrics are tracked using TensorBoard and MLFlow.  An ephemeral MLFlow server is set up as on localhost (on Colab), with UI exposed on the internet using ngrok (http://ngrok.com).  A screenshot (MLFlow_UI_Screenshot.png) of the MLFLow UI webpage from training is included in the repository.

### Service deployment and usage instructions

- docker-compose.yml file is provided in the repository.  It pulls, starts and connects the two containers, yutee/flask-client-catdogbreedclf (the "frontend" container) and  yutee/fastapi-yolov5-catdogbreedclf (the "backend" container).  The "frontend" and "backend" containers have ports 5000 (service running on port 5000 in the container) and 8080 (service running on port 8000 in the container) forwarded to localhost respectively.  Two docker networks ("front-tier" and "back-tier") are specified, with the two containers connected by the "back-tier" network whhich they share.

- Dockerfile for building yutee/flask-client-catdogbreedclf and yutee/fastapi-yolov5-catdogbreedclf can be found in respective zip files.

- Asynchronous API calls supported through the use of FastAPI and Starlette.  This choice for supporting asynchronous calls (vs. a heavier solution based on Celery, for example), is appropriate as the API calls for inference on trained models would be I/O bound rather than CPU bound.

- Client for service - a simple client is served by yutee/flask-client-catdogbreedclf (port forwarding to http://localhost:5000).  Besides that, one can try out the APIs served by yutee/fastapi-yolov5-catdogbreedclf using the "Try It Out" feature (port forwarding to localhost:8080, access by URL: http://localhost:8080/docs#)

- Model - refer to Roboflow_Train_YOLOv5_oxfordBreedID_MLFlow.ipynb for the model print as well as the training code (modified to incorporate MLFLow logging).  


